Type of API calls:
-------------------
- Affiliates makes api call to Hydrogen


API calls: 
----------
- From Affiliates to Hydrogen Server:

  * Hydrogen server will provide an api auth token to make api calls to it.
  * Every request will have auth token in the request header like this:
    Authorization: JWT <auth_token>.

**- Api for Getting list of outlets on given station :**

**https://&lt;irctc-base-url>/api/v1/station/outlets/affiliate/?stationCode=<station_code>&time=<arrival_time_of_train>**

Example :
https://www.ecatering.irctc.co.in/api/v1/station/outlets/affiliate/?stationCode=GDR&time=09:48

**Response :**

```
{
  "status": "success",
  "message": "",
  "result": [
    {
      "outlet": {
        "id": 1061385,

        "vendor": {
          "id": 1217,
          "name": "P K  Shefi",
          "vendorType": "VENDOR",
          "logoImageUrl": "https://hydrogen-uploads.s3.ap-south-1.amazonaws.com/vendor/logoImageUrl/IMG-20180512-WA0066-2018-05-12-03-54-30.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20180919T092601Z&X-Amz-SignedHeaders=host&X-Amz-Expires=3600&X-Amz-Credential=AKIAILMFMNM7D5KWSLJQ%2F20180919%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Signature=2c2a33306da2207836fa2e3ecb82290fff026ce0e98bedbfc42e9c712033cb5a",
          "active": true,
        },

        "name": "NEETHUS - GDR",
        "active": true,
        "onPremise": false,
        "city": "GUDUR",
        "state": "ANDHRA PRADESH",
        "bookingCutOffTime": 180,
        "cancelCutOffTime": 180,
        "stations": [
          "GDR"
        ],
        "mealType": "VEG_AND_NON_VEG",
        "cuisines": [
          "ANDHRA"
        ],
        "minOrderValue": 100,
        "schedules": [
          {
            "id": 1062929,
            "name": "BREAKFAST",
            "startTime": "07:00:36",
            "endTime": "09:30:36"
          },
          {
            "id": 1090110,
            "name": "LUNCH",
            "startTime": "12:00:43",
            "endTime": "14:30:43"
          },
          {
            "id": 1090111,
            "name": "DINNER",
            "startTime": "18:00:39",
            "endTime": "21:00:39"
          },
          {
            "id": 1090112,
            "name": "WATER BOTTLE",
            "startTime": "06:00:40",
            "endTime": "21:00:40"
          }
        ],
        "deliveryCharge": 0,
        "latitude": null,
        "longitude": null,
        "logoImageUrl": null,
        "createdAt": null,
        "createdBy": null,
        "updatedAt": "2018-06-28 04:56 UTC",
        "updatedBy": "user-226-P K  Shefi"
      },
      "stationCode": "GDR"
    }
  ]
}
```

-  **list menu for a selected outlet:**

    **GET https://&lt;irctc-base-url>/api/v1/vendor/&lt;vendor_id>/outlet/&lt;vendor_id>/menu/active/affiliate/?time=&lt;train_arrival_time>&stationCode=&lt;station_code>&date=&lt;delivery_date>**
    
 **Example:**

https://www.ecatering.irctc.co.in/api/v1/vendor/6997/outlet/7100/menu/active/affiliate/?time=14:15&stationCode=BZA&date=2018-09-21


**Response**
    
```
{
  "status": "success",
  "message": "",

  "result": {
    "id": 8778,
    "active": true,
    "deliveryCharge": {
      "charge": 0,
      "tax": 0,
      "totalCharge": 0
    },

    "menuItems": [
      {
        "id": 44014,
        "itemName": "KOSALA SPL VEG THALI",
        "description": "Tawa Roti X3, Jeera Rice, Paneer Dish, Seasonal Veg, Dal Fry, Sweet, Salad, Pickle.",
        "basePrice": 170,
        "sellingPrice": 178.5,
        "taxRate": 8.5,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "THALI",
        "cuisine": "NORTH_INDIAN",
        "options": [],
        "baseOption": null,
        "createdAt": null,
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-09-15 10:08 UTC",
        "updatedBy": "user-251-HOTEL KOSALA"
      },
      {
        "id": 44026,
        "itemName": "MINI VEG THALI",
        "description": "2-Tawa Roti, Plain Rice, Seasonal Veg, Dal Fry, Salad & Pickle.",
        "basePrice": 90,
        "sellingPrice": 94.5,
        "taxRate": 4.5,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "THALI",
        "cuisine": "NORTH_INDIAN",
        "options": [],
        "baseOption": null,
        "createdAt": null,
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-06-03 06:45 UTC",
        "updatedBy": "user-251-HOTEL KOSALA"
      },
      {
        "id": 44041,
        "itemName": "JAIN VEG THALI",
        "description": "3-Tawa Roti, Jeera Rice, Paneer Dish, Seasonal Veg, Dal Fry, Sweet, Salad & Pickle",
        "basePrice": 170,
        "sellingPrice": 178.5,
        "taxRate": 8.5,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "THALI",
        "cuisine": "NORTH_INDIAN",
        "options": [],
        "baseOption": null,
        "createdAt": null,
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-09-15 10:08 UTC",
        "updatedBy": "user-251-HOTEL KOSALA"
      },
      {
        "id": 44081,
        "itemName": "KOSALA SOUTH INDIAN VEG THALI",
        "description": "2-Tawa Roti, Plain Rice, Veg Curry, Veg Fry, Dal, Curd, Pickle & Sweet",
        "basePrice": 130,
        "sellingPrice": 136.5,
        "taxRate": 6.5,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "THALI",
        "cuisine": "ANDHRA",
        "options": [],
        "baseOption": null,
        "createdAt": null,
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-06-03 06:45 UTC",
        "updatedBy": "user-251-HOTEL KOSALA"
      },
      {
        "id": 44376,
        "itemName": "ALOO PARATHA COMBO",
        "description": "ALOO PARATHA X 2  with CURD",
        "basePrice": 70,
        "sellingPrice": 73.5,
        "taxRate": 3.5,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 622,
            "name": "Breakfast",
            "startTime": "07:00:17",
            "endTime": "11:00:17"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "MAINS",
        "cuisine": "NORTH_INDIAN",
        "options": [],
        "baseOption": null,
        "createdAt": null,
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-02-11 05:52 UTC",
        "updatedBy": "user-251-HOTEL KOSALA"
      },
      {
        "id": 45317,
        "itemName": "NON VEG THALI",
        "description": "3-Tawa Roti , Plain Rice, Chicken Curry, Seasonal Veg, Dal Fry, Salad, Pickle & Sweet",
        "basePrice": 190,
        "sellingPrice": 199.5,
        "taxRate": 9.5,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": false,
        "tags": null,
        "foodType": "THALI",
        "cuisine": "NORTH_INDIAN",
        "options": [],
        "baseOption": null,
        "createdAt": null,
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-09-15 10:08 UTC",
        "updatedBy": "user-251-HOTEL KOSALA"
      },
      {
        "id": 45461,
        "itemName": "VEG GRILLED SANDWICH",
        "description": "Veg Grilled Sandwich - 2 pc with Sauce",
        "basePrice": 75,
        "sellingPrice": 78.75,
        "taxRate": 3.75,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 622,
            "name": "Breakfast",
            "startTime": "07:00:17",
            "endTime": "11:00:17"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "BREAKFAST",
        "cuisine": "NORTH_INDIAN",
        "options": [],
        "baseOption": null,
        "createdAt": null,
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-09-15 10:09 UTC",
        "updatedBy": "user-251-HOTEL KOSALA"
      },
      {
        "id": 45484,
        "itemName": "CHICKEN GRILLED SANDWICH",
        "description": "Chicken Grilled Sandwich - 2 pcs with Sauce",
        "basePrice": 90,
        "sellingPrice": 94.5,
        "taxRate": 4.5,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 622,
            "name": "Breakfast",
            "startTime": "07:00:17",
            "endTime": "11:00:17"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": false,
        "tags": null,
        "foodType": "BREAKFAST",
        "cuisine": "NORTH_INDIAN",
        "options": [],
        "baseOption": null,
        "createdAt": null,
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-09-15 10:09 UTC",
        "updatedBy": "user-251-HOTEL KOSALA"
      },
      {
        "id": 45523,
        "itemName": "HYDERABADI CHICKEN DHUM BIRYANI",
        "description": "A perfectly made bowl of Hyderabadi Chicken Dum Biriyani can elevate to levels that's close to ecstasy - 650 Gms packed with Katta & Raitha",
        "basePrice": 240,
        "sellingPrice": 252,
        "taxRate": 12,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": false,
        "tags": null,
        "foodType": "MAINS",
        "cuisine": "MUGHALAI",
        "options": [],
        "baseOption": null,
        "createdAt": "2018-02-06 12:12 UTC",
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-02-06 12:12 UTC",
        "updatedBy": null
      },
      {
        "id": 45545,
        "itemName": "MINI CHICKEN BIRYANI",
        "description": "Chicken Biryani - 400 Gms packed with Katta & Raitha",
        "basePrice": 140,
        "sellingPrice": 147,
        "taxRate": 7,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": false,
        "tags": null,
        "foodType": "MAINS",
        "cuisine": "ANDHRA",
        "options": [],
        "baseOption": null,
        "createdAt": "2018-02-06 12:14 UTC",
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-02-06 12:14 UTC",
        "updatedBy": null
      },
      {
        "id": 45566,
        "itemName": "VEG FRIED RICE",
        "description": "Veg Fried Rice - 400 Gms packed with sauce",
        "basePrice": 140,
        "sellingPrice": 147,
        "taxRate": 7,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "MAINS",
        "cuisine": "CHINESE",
        "options": [],
        "baseOption": null,
        "createdAt": null,
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-09-15 10:09 UTC",
        "updatedBy": "user-251-HOTEL KOSALA"
      },
      {
        "id": 45588,
        "itemName": "PULKHA",
        "description": "Pulkha X 2",
        "basePrice": 30,
        "sellingPrice": 31.5,
        "taxRate": 1.5,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "MAINS",
        "cuisine": "PUNJABI",
        "options": [],
        "baseOption": null,
        "createdAt": "2018-02-06 12:18 UTC",
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-02-06 12:18 UTC",
        "updatedBy": null
      },
      {
        "id": 45592,
        "itemName": "BUTTER NAAN",
        "description": "Butter Naan - 2 pc",
        "basePrice": 35,
        "sellingPrice": 36.75,
        "taxRate": 1.75,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "MAINS",
        "cuisine": "PUNJABI",
        "options": [],
        "baseOption": null,
        "createdAt": "2018-02-06 12:19 UTC",
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-02-06 12:19 UTC",
        "updatedBy": null
      },
      {
        "id": 45737,
        "itemName": "CURD RICE",
        "description": "Curd Rice- 300 Gm with Pickle",
        "basePrice": 65,
        "sellingPrice": 68.25,
        "taxRate": 3.25,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "MAINS",
        "cuisine": "ANDHRA",
        "options": [],
        "baseOption": null,
        "createdAt": "2018-02-06 12:33 UTC",
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-02-06 12:33 UTC",
        "updatedBy": null
      },
      {
        "id": 45763,
        "itemName": "CHICKEN FRIED RICE",
        "description": "Chicken Fried Rice-400 Gm with Sauce",
        "basePrice": 160,
        "sellingPrice": 168,
        "taxRate": 8,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": false,
        "tags": null,
        "foodType": "MAINS",
        "cuisine": "CHINESE",
        "options": [],
        "baseOption": null,
        "createdAt": null,
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-09-15 10:10 UTC",
        "updatedBy": "user-251-HOTEL KOSALA"
      },
      {
        "id": 45781,
        "itemName": "VEG HAKKA NOODLES",
        "description": "Veg Noodels - 400 Gm packed with Sauce",
        "basePrice": 120,
        "sellingPrice": 126,
        "taxRate": 6,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "MAINS",
        "cuisine": "CHINESE",
        "options": [],
        "baseOption": null,
        "createdAt": null,
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-04-09 11:03 UTC",
        "updatedBy": "user-251-HOTEL KOSALA"
      },
      {
        "id": 45791,
        "itemName": "CHICKEN HAKKA NOODLES",
        "description": "Chicken Noodles - 400 Gm Packed with sauce",
        "basePrice": 130,
        "sellingPrice": 136.5,
        "taxRate": 6.5,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": false,
        "tags": null,
        "foodType": "MAINS",
        "cuisine": "CHINESE",
        "options": [],
        "baseOption": null,
        "createdAt": null,
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-04-09 11:02 UTC",
        "updatedBy": "user-251-HOTEL KOSALA"
      },
      {
        "id": 45816,
        "itemName": "ROTI PANEER COMBO",
        "description": "Roti x 3, Paneer Butter Masala (250g)",
        "basePrice": 120,
        "sellingPrice": 126,
        "taxRate": 6,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "COMBO",
        "cuisine": "PUNJABI",
        "options": [],
        "baseOption": null,
        "createdAt": "2018-02-06 12:39 UTC",
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-02-06 12:39 UTC",
        "updatedBy": null
      },
      {
        "id": 45822,
        "itemName": "ROTI SEASONAL VEG COMBO",
        "description": "Roti x 3, Seasonal Veg Curry (250g)",
        "basePrice": 110,
        "sellingPrice": 115.5,
        "taxRate": 5.5,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "COMBO",
        "cuisine": "PUNJABI",
        "options": [],
        "baseOption": null,
        "createdAt": "2018-02-06 12:41 UTC",
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-02-06 12:41 UTC",
        "updatedBy": null
      },
      {
        "id": 45856,
        "itemName": "ROTI CHICKEN COMBO",
        "description": "Roti x 3, Chicken Curry (250g)",
        "basePrice": 130,
        "sellingPrice": 136.5,
        "taxRate": 6.5,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": false,
        "tags": null,
        "foodType": "COMBO",
        "cuisine": "MUGHALAI",
        "options": [],
        "baseOption": null,
        "createdAt": "2018-02-06 12:43 UTC",
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-02-06 12:43 UTC",
        "updatedBy": null
      },
      {
        "id": 45934,
        "itemName": "JEERA RICE COMBO",
        "description": "Jeera Rice (350g) packed with Dal Fry (250g)",
        "basePrice": 110,
        "sellingPrice": 115.5,
        "taxRate": 5.5,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "COMBO",
        "cuisine": "NORTH_INDIAN",
        "options": [],
        "baseOption": null,
        "createdAt": null,
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-02-11 05:54 UTC",
        "updatedBy": "user-251-HOTEL KOSALA"
      },
      {
        "id": 51422,
        "itemName": "WHITE KALAKAND",
        "description": "White Kalakand  /Indian Milk Sweet- 250Gm",
        "basePrice": 100,
        "sellingPrice": 105,
        "taxRate": 5,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "DESSERTS",
        "cuisine": "NORTH_INDIAN",
        "options": [],
        "baseOption": null,
        "createdAt": "2018-02-07 10:25 UTC",
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-02-07 10:25 UTC",
        "updatedBy": null
      },
      {
        "id": 51459,
        "itemName": "ASSORTED MILK SWEETS",
        "description": "Assorted kova sweets - 250Gm",
        "basePrice": 100,
        "sellingPrice": 105,
        "taxRate": 5,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "DESSERTS",
        "cuisine": "NORTH_INDIAN",
        "options": [],
        "baseOption": null,
        "createdAt": "2018-02-07 10:27 UTC",
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-02-07 10:27 UTC",
        "updatedBy": null
      },
      {
        "id": 51475,
        "itemName": "BUTTER MILK",
        "description": "Butter Milk",
        "basePrice": 25,
        "sellingPrice": 26.25,
        "taxRate": 1.25,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "BEVERAGE",
        "cuisine": "NORTH_INDIAN",
        "options": [],
        "baseOption": null,
        "createdAt": "2018-02-07 10:28 UTC",
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-02-07 10:28 UTC",
        "updatedBy": null
      },
      {
        "id": 51485,
        "itemName": "SWEET LASSI",
        "description": "Sweet Lassi",
        "basePrice": 35,
        "sellingPrice": 36.75,
        "taxRate": 1.75,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "BEVERAGE",
        "cuisine": "NORTH_INDIAN",
        "options": [],
        "baseOption": null,
        "createdAt": "2018-02-07 10:29 UTC",
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-02-07 10:29 UTC",
        "updatedBy": null
      },
      {
        "id": 51510,
        "itemName": "PANEER BUTTER MASALA",
        "description": "Paneer Butter Masala - 300G",
        "basePrice": 120,
        "sellingPrice": 126,
        "taxRate": 6,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "MAINS_GRAVY",
        "cuisine": "PUNJABI",
        "options": [],
        "baseOption": null,
        "createdAt": "2018-02-07 10:31 UTC",
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-02-07 10:31 UTC",
        "updatedBy": null
      },
      {
        "id": 51513,
        "itemName": "MIX VEG CURRY",
        "description": "Mix Veg Curry - 300Gm",
        "basePrice": 110,
        "sellingPrice": 115.5,
        "taxRate": 5.5,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "MAINS_GRAVY",
        "cuisine": "PUNJABI",
        "options": [],
        "baseOption": null,
        "createdAt": "2018-02-07 10:32 UTC",
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-02-07 10:32 UTC",
        "updatedBy": null
      },
      {
        "id": 51531,
        "itemName": "CHILLY CHICKEN",
        "description": "Chilly Chicken - 300Gm",
        "basePrice": 150,
        "sellingPrice": 157.5,
        "taxRate": 7.5,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": false,
        "tags": null,
        "foodType": "MAINS_GRAVY",
        "cuisine": "CHINESE",
        "options": [],
        "baseOption": null,
        "createdAt": null,
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-02-07 10:34 UTC",
        "updatedBy": "user-251-HOTEL KOSALA"
      },
      {
        "id": 101980,
        "itemName": "FISH BIRYANI- Cooked with Tendered pieces of Fish & Spices ",
        "description": "Fish Biryani-650Gms packed with katta & raitha",
        "basePrice": 300,
        "sellingPrice": 315,
        "taxRate": 15,
        "active": true,
        "schedules": [
          {
            "id": 621,
            "name": "Lunch",
            "startTime": "10:00:16",
            "endTime": "22:30:16"
          },
          {
            "id": 7110,
            "name": "Dinner",
            "startTime": "19:00:33",
            "endTime": "22:00:33"
          }
        ],
        "isVegetarian": true,
        "tags": null,
        "foodType": "MAINS",
        "cuisine": "MUGHALAI",
        "options": [],
        "baseOption": null,
        "createdAt": null,
        "createdBy": "user-251-HOTEL KOSALA",
        "updatedAt": "2018-02-14 08:44 UTC",
        "updatedBy": "user-251-HOTEL KOSALA"
      }
    ],
    "createdAt": null,
    "createdBy": null,
    "updatedAt": null,
    "updatedBy": null
  }
}
```
**- API to add customer in IRCTC system**

**https://www.ecatering.irctc.co.in/api/v1/affiliate/customer**

**Request JSON:-**

```
{"fullName":"Nikhil Mishra","mobile":"9716220000","email":"nikhil6.mishra@gmail.com"}
```

**Response JSON**

```
{
  "status": "success",
  "message": "",
  "result": {
    "id": 21298,
    "fullName": "Nikhil Mishra",
    "email": "nikhil6.mishra@gmail.com",
    "mobile": "9716221914",
    "gender": null,
    "profilePictureUrl": null,
    "emailVerified": false,
    "mobileVerified": false
  }
}
```
**API for placing order**

**https://www.ecatering.irctc.co.in/api/v1/order/affiliate**

Request Json :-

```
{
  "customer": {
    "id": 21298
  },
  "outlet": {
    "id": 7100,
    "vendor": {
      "id": 6997
    }
  },
  "pnr": "4703398951",
  "trainNo": "08302",
  "trainName": "BAND SBP EXP",
  "stationName": "VIJAYAWADA JN",
  "stationCode": "BZA",
  "berth": "48",
  "coach": "S5",
  "orderItems": [
    {
      "itemId": "45317",
      "quantity": 1
    },
    {
      "itemId": "51459",
      "quantity": 1
    }
  ],
  "deliveryDate": "2018-09-21 14:15 IST",
  "bookingDate": "2018-09-19 16:28 IST",
  "orderFrom": "desktop web",
  "paymentStatus" : "CASH_ON_DELIVERY"
}
```
**Response JSON**

```
{
  "status": "success",
  "message": "",
  "result": {
    "id": 3492511,
    "orderNo": 0,
    "orderItems": [
      {
        "itemId": 45317,
        "itemName": "NON VEG THALI",
        "description": "3-Tawa Roti , Plain Rice, Chicken Curry, Seasonal Veg, Dal Fry, Salad, Pickle & Sweet",
        "basePrice": 190,
        "sellingPrice": 199.5,
        "taxRate": 9.5,
        "isVegetarian": false,
        "quantity": 1,
        "margin": 0.12,
        "status": null,
        "option": null,
        "toppings": null,
        "crust": null,
        "size": null,
        "foodType": null,
        "remarks": null,
        "discount": null,
        "discountedPrice": null
      },
      {
        "itemId": 51459,
        "itemName": "ASSORTED MILK SWEETS",
        "description": "Assorted kova sweets - 250Gm",
        "basePrice": 100,
        "sellingPrice": 105,
        "taxRate": 5,
        "isVegetarian": true,
        "quantity": 1,
        "margin": 0.12,
        "status": null,
        "option": null,
        "toppings": null,
        "crust": null,
        "size": null,
        "foodType": null,
        "remarks": null,
        "discount": null,
        "discountedPrice": null
      }
    ],
    "outlet": {
      "id": 7100,
      "vendor": {
        "id": 6997,
        "name": "HOTEL KOSALA",
        "vendorType": "VENDOR",
      },
      "name": "HOTEL KOSALA"
    },
    "customer": {
      "id": 21298,
      "fullName": "Nikhil Mishra",
      "email": "nikhil6.mishra@gmail.com",
      "mobile": "9716221914"
    },
    "pnr": "4703398951",
    "trainNo": "08302",
    "trainName": "BAND SBP EXP",
    "stationName": "VIJAYAWADA JN",
    "stationCode": "BZA",
    "coach": "S5",
    "berth": "48",
    "bookingDate": "2018-09-19 10:58 UTC",
    "deliveryDate": "2018-09-21 08:45 UTC",
    "totalAmount": 304.5,
    "gst": 14.5,
    "amountPayable": 305,
    "status": "ORDER_PLACED",
    "remarks": "NA",
    "source": null,
    "paymentType": "CASH_ON_DELIVERY",
    "orderFrom": "desktop web",
    "orderPayment": null,
    "createdAt": "2018-09-19 10:58 UTC",
    "updatedAt": "2018-09-19 10:58 UTC",
    "otp": "2039",
    "vendorOrderId": null,
    "deliveryCharge": 0,
    "eta": "2018-09-21 08:45 UTC",
    "deliveryStatus": null,
    "discountAmount": null
  }
}
```
**API For Getting Order Details Against a PNR no.**

**https://www.ecatering.irctc.co.in/api/v1/order/affiliate?page=1&size=10&pnr=<PNR_NO>**

```
{
  "status": "success",
  "message": "",
  "result": {
    "total": 1,
    "orders": [
      {
        "id": 3492511,
        "orderNo": 0,
        "orderItems": [
          {
            "itemId": 45317,
            "itemName": "NON VEG THALI",
            "description": "3-Tawa Roti , Plain Rice, Chicken Curry, Seasonal Veg, Dal Fry, Salad, Pickle & Sweet",
            "basePrice": 190,
            "sellingPrice": 199.5,
            "taxRate": 9.5,
            "isVegetarian": false,
            "quantity": 1,
            "margin": 0.12,
            "status": null,
            "option": null,
            "toppings": null,
            "crust": null,
            "size": null,
            "foodType": null,
            "remarks": null,
            "discount": null,
            "discountedPrice": null
          },
          {
            "itemId": 51459,
            "itemName": "ASSORTED MILK SWEETS",
            "description": "Assorted kova sweets - 250Gm",
            "basePrice": 100,
            "sellingPrice": 105,
            "taxRate": 5,
            "isVegetarian": true,
            "quantity": 1,
            "margin": 0.12,
            "status": null,
            "option": null,
            "toppings": null,
            "crust": null,
            "size": null,
            "foodType": null,
            "remarks": null,
            "discount": null,
            "discountedPrice": null
          }
        ],
        "outlet": {
          "id": 7100,
          "vendor": {
            "id": 6997,
            "name": "HOTEL KOSALA",
            "vendorType": "VENDOR"
          },
          "name": "HOTEL KOSALA"
        },
        "customer": {
          "id": 21298,
          "fullName": "Nikhil Mishra",
          "email": "nikhil6.mishra@gmail.com",
          "mobile": "9716221914"
        },
        "pnr": "4703398951",
        "trainNo": "08302",
        "trainName": "BAND SBP EXP",
        "stationName": "VIJAYAWADA JN",
        "stationCode": "BZA",
        "coach": "S5",
        "berth": "48",
        "bookingDate": "2018-09-19 10:58 UTC",
        "deliveryDate": "2018-09-21 08:45 UTC",
        "totalAmount": 304.5,
        "gst": 14.5,
        "amountPayable": 305,
        "status": "ORDER_PLACED",
        "remarks": "NA",
        "source": null,
        "paymentType": "CASH_ON_DELIVERY",
        "orderFrom": "desktop web",
        "orderPayment": null,
        "createdAt": "2018-09-19 10:58 UTC",
        "updatedAt": "2018-09-19 10:58 UTC",
        "otp": "2039",
        "vendorOrderId": null,
        "deliveryCharge": 0,
        "eta": "2018-09-21 08:45 UTC",
        "deliveryStatus": null,
        "discountAmount": null
      }
    ]
  }
}
```






**Note:** 

1) More than two non cancelled orders with same PNR and at the same can't be booked.

Response :-
```
{
"status": "failure",
"message": "error: Order already exists for PNR NO 1876516712 on station TIRUPATI you can book maximum two orders on one station within a journey",
"result": {}
}
```
2. Within the span of 5 minutes two orders with same PNR and station code can't be placed.If first is not cancelled.

Response:-
```
{
"status": "failure",
"message": "error: Duplicate Order",
"result": {}
}
```
